#!/bin/sh

. /usr/local/bin/docker-env

if [ "${NO_GT}" = "N" ]; then
	exec /app/gt/node_modules/.bin/gt $([ ! -z "${GT_IP}" ] && echo "--ip ${GT_IP}" | sed -e 's/,/ --ip /g') $([ ! -z "${GT_PORT}" ] && echo "--port ${GT_PORT}") $([ ! -z "${GT_DNSTYPE}" ] && echo "--dns-type ${GT_DNSTYPE}") $([ ! -z "${GT_DNSSERVER}" ] && echo "--dns-server ${GT_DNSSERVER}")
fi

