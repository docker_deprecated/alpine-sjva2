#!/bin/sh

. /usr/local/bin/docker-env

if [ "${NO_CELERY}" = "N" ]; then
	while [ \( "${NO_GT}" = "N" -a -z "$(pgrep -f "node .*/gt")" \) \
		-o \( -z "$(pgrep -f "redis-server")" \) ]
	do
		sleep 1
	done

	cd "${SJVA2_HOME}"
	if [ ! -z "${SJVA2_VENV}" ]; then
		exec python2 -OO ${SJVA2_VENV}/bin/celery worker -A sjva.celery --loglevel=info
	else
		exec python2 -OO /usr/bin/celery worker -A sjva.celery --loglevel=info
	fi
fi

