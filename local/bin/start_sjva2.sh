#!/bin/sh

. /usr/local/bin/docker-env

while [ \( "${NO_GT}" = "N" -a -z "$(pgrep -f "node .*/gt")" \) \
	-o \( "${NO_CELERY}" = "N" -a -z "$(pgrep -f "redis-server")" \) \
	-o \( "${NO_CELERY}" = "N" -a -z "$(pgrep -f "python2 -OO .*/bin/celery worker")" \) ]
do
	sleep 1
done

cd ${SJVA2_HOME}

COUNT=0
while [ 1 ];
do
	#Update Logic을
	#echo "Updating SVJA2"
	#wget --no-check-certificate https://github.com/soju6jan/SJVA2/archive/master.tar.gz -q -O - | tar zx -C ${SJVA2_HOME}
	#rm -rf ${SJVA2_HOME}/*.sh ${SJVA2_HOME}/*.cmd
	#update
	find . -name "index.lock" -exec rm -f {} \;
	git reset --hard HEAD
	git pull
	chmod 777 .
	chmod -R 777 ./bin
	if [ -d "./data/custom" ]; then
		chmod -R +x ./data/custom
	fi
	#remove other architecture
	case "$(uname -m)" in
		x86_64)
			rm -rf bin/Darwin bin/Windows bin/LinuxArm
			ln -sf Linux/filebrowser bin/filebrowser
			ln -sf Linux/rclone bin/rclone
			;;
		aarch64)
			rm -rf bin/Darwin bin/Windows bin/Linux bin/LinuxArm
			mkdir -p bin/LinuxAarch64
			if [ ! -e bin/LinuxAarch64/rclone ]; then
				wget --no-check-certificate -q -O - https://rclone.org/install.sh | bash
				mv /usr/bin/rclone bin/LinuxAarch64/
				chmod 755 bin/LinuxAarch64/rclone
			fi
			if [ ! -e bin/LinuxAarch64/filebrowser ]; then
				wget --no-check-certificate -q -O - https://filebrowser.xyz/get.sh | bash
				mv /usr/local/bin/filebrowser bin/LinuxAarch64
				chmod 755 bin/LinuxAarch64/filebrowser
			fi
			ln -sf Linux/filebrowser bin/filebrowser
			ln -sf Linux/rclone bin/rclone
			;;
		armv7l)
			rm -rf bin/Darwin bin/Windows bin/Linux
			ln -sf LinuxArm/filebrowser bin/filebrowser
			ln -sf LinuxArm/rclone bin/rclone
			;;
		*)
			;;
	esac
	if [ -e update_requirements.txt ]; then
		pip2 install -r update_requirements.txt
	fi
	if [ -e migrations ]; then
		python2 -OO -m flask db migrate || true &> /dev/null
		python2 -OO -m flask db upgrade || true
	fi
	python2 -OO sjva.py 0 ${COUNT} $([ "${NO_CELERY}" = "Y" ] && echo "no_celery") $([ "${IS_DEBUG}" = "Y" ] && echo "debug")
	RESULT=$?
	echo "PYTHON EXIT CODE : ${RESULT}.............."
	if [ "$RESULT" = "0" ]; then
		echo 'FINISH....'
		break
	else
		echo 'REPEAT....'
	fi
	COUNT=`expr $COUNT + 1`
done

