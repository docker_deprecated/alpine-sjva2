#!/bin/sh

. /usr/local/bin/docker-env

if [ "${NO_FILEBROWSER}" = "N" ]; then
	cd "${SJVA2_HOME}"
	exec ${SJVA2_HOME}/bin/filebrowser -a 0.0.0.0 -p ${FILEBROWSER_PORT} -r / -d ${SJVA2_HOME}/data/db/filebrowser.db
fi

