#!/bin/sh

. /usr/local/bin/docker-env

if [ "${NO_CELERY}" = "N" ]; then
	exec redis-server --port ${REDIS_PORT}
fi

