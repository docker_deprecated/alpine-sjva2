# alpine-sjva2

#### [alpine-x64-sjva2](https://hub.docker.com/r/forumi0721alpinex64/alpine-x64-sjva2/)
![Docker Image Version (tag latest semver)](https://img.shields.io/docker/v/forumi0721alpinex64/alpine-x64-sjva2/latest)
![Docker Image Size (tag)](https://img.shields.io/docker/image-size/forumi0721alpinex64/alpine-x64-sjva2/latest)
![MicroBadger Layers (tag)](https://img.shields.io/microbadger/layers/forumi0721alpinex64/alpine-x64-sjva2/latest)
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpinex64/alpine-x64-sjva2)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpinex64/alpine-x64-sjva2)



----------------------------------------
#### Description

* Distribution : [Alpine Linux](https://alpinelinux.org/)
* Architecture : x64,aarch64,armhf
* Appplication : [SJVA2](https://sjva.me/)
    - SVJA2 is  ...



----------------------------------------
#### Run

```sh
docker run -d \
           -p 9999:9999/tcp \
           -p 9998:9998/tcp \
           -v /conf.d:/conf.d \
           forumi0721alpinex64/alpine-x64-sjva2:latest
```



----------------------------------------
#### Usage

* URL : [http://localhost:9999/](http://localhost:9999/)
    - Default username/password : svja2/sjva2



----------------------------------------
#### Docker Options

| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Ports

| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| 9999/tcp           | SJVA2 port                                       |
| 9998/tcp           | FileBrowser port                                 |


#### Volumes

| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| /conf.d            | Persistent data                                  |


#### Environment Variables

| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| IS_DEBUG           | Execute debug mode                               |
| NO_CELERY          | Do not run celery                                |
| NO_FILEBROWSER     | Do not run filebrowser                           |
| NO_GT              | Do not run greentunnel                           |

