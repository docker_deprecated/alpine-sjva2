ARG BUILD_ARCH=x64

FROM forumi0721alpinex64build/alpine-x64-sjva2-builder:latest as builder

FROM forumi0721alpine${BUILD_ARCH}build/alpine-${BUILD_ARCH}-greentunnel-builder:latest as gt

FROM forumi0721/alpine-${BUILD_ARCH}-base:latest

LABEL maintainer="forumi0721@gmail.com"

COPY --from=builder /output /app

COPY --from=gt /output /app

COPY local/. /usr/local/

#RUN ["docker-build-start"]

RUN ["docker-init"]

#RUN ["docker-build-end"]

ENTRYPOINT ["docker-run"]

EXPOSE 9999/tcp 9998/tcp

VOLUME /conf.d

